# RSSupportIOWrapper Android library

[Android Support Library v8](http://developer.android.com/tools/support-library/features.html#v8) provides support for RenderScript on older API. However, `USAGE_IO_INPUT` and `USAGE_IO_OUTPUT` flags were not supported until the release of API 23, where `libRSSupportIO.so` got introduced inside the [SDK prebuilts section](https://android.googlesource.com/platform/prebuilts/sdk/+/marshmallow-release/renderscript/lib/arm/).

RSSupportIOWrapper library embeds these files, so that you can just import the `.aar` file and use it inside your projects. 

You can use the library in two ways: 

* Importing the pre-generated `.aar` file. This is the fastest option.
* Build it your way. This will require you to download the [Android NDK](http://developer.android.com/tools/sdk/ndk/index.html) (> 300MB). In this case, clone the project, open it inside Android Studio and build it (using **Build -> Build APK** menu command): the process will replace the pre-generated file inside **dist** folder.

## Import .aar file

To import the library, using our pre-generated `.aar` file:

* Right click on your main project and click on **New -> Module**.
* Select **Import .JAR/.AAR Package** and click on **Next**.
* Select our **rssupportiowrapper-release.aar**, located inside our **dist** folder.
* Open your main project settings by right-clicking on it and selecting **Open Module Settings**.
* Switch to the **Dependencies** tab and click on **Add -> Module dependency**.
* Select the newly generated **rssupportiowrapper** module and click on **OK**.
* **Clean and rebuild** your project. When you will then package your `.apk`, the `libRSSupportIO.so` will be embedded automatically.

## Reference images

### New module

![](doc/img/newmodule.png)

### Import .JAR/.AAR Package

![](doc/img/importaar.png)

### Open Module Settings

![](doc/img/openmodulesettings.png)

### Add -> Module dependency

![](doc/img/addmoduledependency.png)

### Choose Modules

![](doc/img/choosemodule.png)
